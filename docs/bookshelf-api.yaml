openapi: 3.0.0
info:
  title: Bookshelf API
  description: REST API for testing training
  version: 0.0.1

servers:
  - url: http://localhost:8080
    description: Local docker image

paths:
  /:
    get:
      summary: Endpoint registry
      security: []
      responses:
        '200':
          description: Endpoint registry details
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Endpoints'

  /users:
    post:
      summary: Creates a user account
      security: []
      requestBody:
        description: Creates user account
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
      responses: 
        '201':
          description: User account successfully created.
          content: 
            application/json:
              schema:
                $ref: '#/components/schemas/UserLinks'
        '400':
          description: |
            Bad request, provided request body does not met the requirements.  
            Please check the error message.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '409':
          description: |
            Conflict, user of provided name already exists.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /users/{username}:
    delete:
      summary: Removes user account
      security:
        - BearerAuth: []
      parameters:
        - in: path
          name: username
          required: true
          schema:
            type: string
      responses:
        '204':
          description: | 
            User account has been removed. This operation cannot be undone.
        '401':
          description: Unauthorized. Invalid or no credentials provided.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: | 
            User authenticated by the token is not allowed to delete 
            the account.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
              
  /users/{username}/token:
    get:
      summary: Retuns authentication token of the user
      security: 
        - BasicAuth: []
      parameters:
        - in: path
          name: username
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: string
        '404':
          description: Not Found


  /book:
    get:
      summary: Returns list of books
      security:
        - BearerAuth: []
      responses:
        '200':
          description: |
            OK. Returns list of books added by user.
            If no user token is provided list of public books is returned.
          content:
            application/json:
              schema:
                type: array
                items: 
                  $ref: '#/components/schemas/Book'
    
    post:
      summary: Adds a new book for a user
      security:
        - BearerAuth: []
      requestBody:
        description: Adds a new book for a user
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Book'
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          

  /book/{isbn}:
    get:
      summary: Returns book of given ISBN
      security:
        - BearerAuth: []
      parameters:
        - in: path
          name: isbn
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

    put:
      summary: Updates book of given ISBN
      security:
        - BearerAuth: []
      requestBody:
        description: Adds a new book for a user
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Book'
      parameters:
        - in: path
          name: isbn
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    
    delete:
      summary: Removes book of given ISBN
      security:
        - BearerAuth: []
      parameters:
        - in: path
          name: isbn
          required: true
          schema:
            type: string
      responses:
        '204':
          description: | 
            User account has been removed. This operation cannot be undone.
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  

components:
  schemas:

    Book:
      type: object
      required:
        - isbn
        - title
        - authors
      properties:
        isbn:
          type: string
        title:
          type: string
        authors:
          type: array
          items:
            type: string
        page_count:
          type: integer
        public:
          type: boolean
        owner:
          type: string

    Endpoints:
      type: object
      required:
        - users
        - book
      properties:
        users:
          type: string
          format: uri
        book:
          type: string
          format: uri

    User:
      type: object
      required:
        - username
        - password
        - email
        - age
      properties:
        username:
          type: string
          pattern: '^[a-z]+$'
        password:
          type: string
          format: password
        email:
          type: string
        age:
          type: integer

    UserLinks:
      type: object
      required:
        - self
        - token
      properties:
        self:
          type: string
          format: uri
        token:
          type: string
          format: uri
    
    Error:
      type: object
      required:
        - message
      properties:
        message:
          type: string

  securitySchemes:
    BasicAuth:
      type: http
      scheme: basic
    BearerAuth:
      type: http
      scheme: bearer
      